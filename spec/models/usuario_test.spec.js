var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
//var server = require('../../bin/www');

describe('Testing usuario', function(){

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done){
        Usuario.deleteMany({}, function(err, succes){
            if (err) console.log(err);
            done();
        });
    });

    describe('Usuario.add', ()=>{
        it('agrega solo un usuario', (done) => {
            var aUsuario = new Usuario({nombre: "urbana"});
            Usuario.add(aUsuario, function(err, newUser){
                if (err) console.log(err);
                Usuario.allUsers(function(err, user){
                    console.log(user);
                    expect(user.length).toBe(1);
                    //expect(user[0].nombre).toEqual(aUsuario.nombre);
                    done();
                });
            });
        });
    });


});