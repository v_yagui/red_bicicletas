const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');

passport.use(new LocalStrategy(
    {usernameField: 'email', passwordfield: 'password'},
    function(email, password, done){
        Usuario.findOne({email:email}, function(err,usuario){
            if(err) return done(err);
            if(!usuario) return done(null,false, {message: 'Email no existente o incorrecto'});
            if(!usuario.validPassword(password)) return done(null,false,{message: 'Password incorrecto'});

            return done(null,usuario);
        });
    }
));

passport.serializeUser(function(user,cb){
    cb(null,user.id);
});

passport.deserializeUser(function(user,cb){
    Usuario.findById(user.id,function(err,usuario){
        cb(err,usuario);
    });
});

module.exports = passport;