var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.find({}, function(err, bicicletas){
        res.status(200).json({
            bicicletas: bicicletas
        })
    })
}

exports.bicicleta_create = function (req, res) {
    
    var ubicacion = [req.body.lat, req.body.lng];
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion});
    
    bici.save(function(err){
        res.status(200).json(bici);
    });
}

exports.bicicleta_update = function(req, res){

    var bici = Bicicleta.findByIdAndUpdate(
        {_id: req.body._id},

        {code: req.body.code,
        color: req.body.color, 
        modelo: req.body.modelo, 
        ubicacion: [req.body.lat, req.body.lng]}, 

        function(err){
            res.status(200).send();
        });
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.code, function(err){
        res.status(204).send();
    });

}